
# Configure anycast gateway mac address
fabric forwarding anycast-gateway-mac 0012.3456.1111


# Create TENANT01 local VLAN (only DC1) that will be the anycast gateway for servers directly connected and associate the Layer 2 VNI 
vlan 1001
  name L2_TENANT01
  vn-segment 100001
  
# Create TENANT02 VLAN (Spans both DC1 and DC2 - Same Subnet)
vlan 1002
  name L2_TENANT02
  vn-segment 100002
  
# Create TENANT01 VLAN for Layer 3 VNI used for inter subnet routing between layer 2 segments. VNI Globally unique
vlan 3001
  name L3_IRB_TENANT01
  vn-segment 300001

# Create TENANT02 VLAN for Layer 3 VNI used for inter subnet routing between layer 2 segments. VNI Globally unique
vlan 3002
  name L3_IRB_TENANT02
  vn-segment 300002

# Route map used to add standard community to all routes advertised form DC1
route-map RM_DC1_COMMUNITY permit 10
  description ### SETTING BGP COMMUNITY ON LOCAL PREFIXES FOR REDISTRIBUTION AT FW ###
  set community 65001:10000 additive
  
# Create the VRF for TENANT01. This is used to connect the L2 and L3 VNIs together
vrf context TENANT01
  vni 300001
  rd auto
  address-family ipv4 unicast
    route-target both auto
    route-target both auto evpn

# Create the VRF for TENANT02.
vrf context TENANT02
  vni 300002
  rd auto
  address-family ipv4 unicast
    route-target both auto
    route-target both auto evpn
vrf context management

# Make TCAM space available for arp suppression. Reduce racl to 512 and give arp-ether 256.
# Arp suppression cache used to hold ip-mac or ip-VTEP associations 
hardware access-list tcam region racl 512
hardware access-list tcam region arp-ether 256 double-wide


# Create TENANT01 SVI that will be the anycast gateway for servers directly connected
interface Vlan1001
  description ### TENANT01 ###
  no shutdown
  vrf member TENANT01
  no ip redirects
  ip address 172.20.1.1/24
  no ipv6 redirects
  fabric forwarding mode anycast-gateway

# Create TENANT02 SVI that will be the anycast gateway for servers directly connected
interface Vlan1002
  description ### TENANT02 ###
  no shutdown
  vrf member TENANT02
  no ip redirects
  ip address 172.22.1.1/24
  no ipv6 redirects
  fabric forwarding mode anycast-gateway

# Create TENANT01 SVI used for inter subnet routing between layer 2 segments. 
interface Vlan3001
  description ### TENANT01 Routing ###
  no shutdown
  vrf member TENANT01
  ip forward

# Create TENANT02 SVI used for inter subnet routing between layer 2 segments. 
interface Vlan3002
  description ### TENANT02 Routing ###
  no shutdown
  vrf member TENANT02
  ip forward

# Create the NVE interface. Used to encap/decap VxLAN and holds the L2 and L3 information. BGP is the reachability protocol
# Use BGP to advertise mac and suppress arp
interface nve1
  no shutdown
  host-reachability protocol bgp
  source-interface loopback1
  member vni 100001
    suppress-arp
    ingress-replication protocol bgp
  member vni 100002
    suppress-arp
    ingress-replication protocol bgp
  member vni 300001 associate-vrf
  member vni 300002 associate-vrf

# Configure interface that connects to the spine switch and enable OSPF
interface Ethernet1/1
  description ### Link to Spine DC1SPINE01 Eth 1/2 ###
  no switchport
  mtu 9216
  ip address 10.1.1.5/31
  ip ospf authentication message-digest
  ip ospf message-digest-key 1 md5 3 e99f306cc637e510ff865d69fbbd0bb8
  ip ospf network point-to-point
  ip router ospf 1 area 1.1.1.1
  no shutdown

# Configure interface that connects to TENANT01 server
interface Ethernet1/2
  description ### TENANT01 Server01 ###
  switchport access vlan 1001

# Configure interface that connects to TENANT02 server
interface Ethernet1/3
  description ### TENANT02 Server01 ###
  switchport access vlan 1002

# Assign OOB OAM IP
interface mgmt0
  vrf member management
  ip address 1.1.1.12/24

# Assign IP to loopback0 and add to OSPF process
interface loopback0
  description ### ROUTER ID ###
  ip address 10.1.255.7/32
  ip router ospf 1 area 1.1.1.1

# Assign Primary and Secondary IP to loopback1 and add to OSPF process
# Loopback used by NVE.
# The secondary IP address (VTEP) is used for all VxLAN traffic that includes multicast and unicast encapsulated traffic
# Secondary required to be same if using vPC across two Nexus
interface loopback1
  description ### VTEP ID ###
  ip address 10.1.100.7/32
  ip address 10.1.100.8/32 secondary
  ip router ospf 1 area 1.1.1.1

# Required on first install on nxosv
boot nxos bootflash:/nxos.9.2.2.bin

# Enable OSPF and configure throttling 
router ospf 1
  router-id 10.1.255.7
  timers throttle spf 10 100 500
  timers throttle lsa 0 100 500
  
# Configure iBGP to SPINE01 (RR)
# Enable address families ipv4 unicast and L2VPN EVPN to advertise EVPN routes. 
# Send extended communities for EVPN
router bgp 65001
  router-id 10.1.255.7
  log-neighbor-changes
  address-family ipv4 unicast
  address-family l2vpn evpn
  neighbor 10.1.255.5
    remote-as 65001
    password 3 e99f306cc637e510ff865d69fbbd0bb8
    update-source loopback0
    address-family ipv4 unicast
    address-family l2vpn evpn
      send-community
      send-community extended
  vrf TENANT01
    address-family ipv4 unicast
      network 172.20.1.0/24 route-map RM_DC1_COMMUNITY
  vrf TENANT02
    address-family ipv4 unicast
      network 172.22.1.0/24 route-map RM_DC1_COMMUNITY
      
# Configure L2 VNIs within EVPN instance to generate EVPN routes from MAC address and ARP tables
evpn
  vni 100001 l2
    rd auto
    route-target import auto
    route-target export auto
  vni 100002 l2
    rd auto
    route-target import auto
    route-target export auto
