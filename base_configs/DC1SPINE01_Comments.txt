# Enable required features
cfs eth distribute
nv overlay evpn
feature ospf
feature bgp
feature pim
feature fabric forwarding
feature interface-vlan
feature vn-segment-vlan-based
feature lacp
feature lldp
feature bfd
feature nv overlay

# Configure interface that connects to the border leaf switch and enable OSPF
interface Ethernet1/1
  description ### Link to border leaf DC1BORDERLEAF01 Eth 1/2 ###
  no switchport
  mtu 9216
  ip address 10.1.1.3/31
  ip ospf authentication message-digest
  ip ospf message-digest-key 1 md5 3 e99f306cc637e510ff865d69fbbd0bb8
  ip ospf network point-to-point
  ip router ospf 1 area 1.1.1.1
  no shutdown

# Configure interface that connects to the leaf switch and enable OSPF
interface Ethernet1/2
  description ### Link to leaf DC1LEAF01 Eth 1/1 ###
  no switchport
  mtu 9216
  ip address 10.1.1.4/31
  ip ospf authentication message-digest
  ip ospf message-digest-key 1 md5 3 e99f306cc637e510ff865d69fbbd0bb8
  ip ospf network point-to-point
  ip router ospf 1 area 1.1.1.1
  no shutdown

# Assign OOB OAM IP
interface mgmt0
  vrf member management
  ip address 1.1.1.11/24

# Assign IP to loopback0 and add to OSPF process
interface loopback0
  description ### ROUTER ID ###
  ip address 10.1.255.5/32
  ip router ospf 1 area 1.1.1.1
  
# Enable OSPF and configure throttling 
router ospf 1
  router-id 10.1.255.5
  timers throttle spf 10 100 500
  timers throttle lsa 0 100 500
  
  # Configure BGP as route reflector server for AS 65001 in DC1 CLOS
router bgp 65001
  router-id 10.1.255.5
  log-neighbor-changes
  address-family ipv4 unicast
  address-family l2vpn evpn
    retain route-target all
  template peer LEAF-PEERS
    remote-as 65001
    password 3 e99f306cc637e510ff865d69fbbd0bb8
    update-source loopback0
    timers 20 60
    address-family ipv4 unicast
      send-community
      send-community extended
      route-reflector-client
      soft-reconfiguration inbound
    address-family l2vpn evpn
      send-community
      send-community extended
      route-reflector-client
  neighbor 10.1.255.2
    inherit peer LEAF-PEERS
  neighbor 10.1.255.7
    inherit peer LEAF-PEERS

