version 9.2(2) Bios:version
hostname DC2BORDERLEAF01
vdc DC2BORDERLEAF01 id 1
  limit-resource vlan minimum 16 maximum 4094
  limit-resource vrf minimum 2 maximum 4096
  limit-resource port-channel minimum 0 maximum 511
  limit-resource u4route-mem minimum 248 maximum 248
  limit-resource u6route-mem minimum 96 maximum 96
  limit-resource m4route-mem minimum 58 maximum 58
  limit-resource m6route-mem minimum 8 maximum 8

cfs eth distribute
nv overlay evpn
feature ospf
feature bgp
feature pim
feature fabric forwarding
feature interface-vlan
feature vn-segment-vlan-based
feature lacp
feature lldp
feature bfd
feature nv overlay

no password strength-check
username admin password 5 $5$/VdGO93.$CQeDOvBhN5pEXMFNjnFmChpLVsyUk7SsNRzu9f.Acv6  role network-admin
username ansible password 5 $5$CneAT6Kb$vc/OXbxvWOt.8U2yJVyjPoxofQ0usYB.ZM2KS6Du4j1  role network-admin
ip domain-lookup
copp profile strict
snmp-server user admin network-admin auth md5 0x6e3cf6073b9580a401b58b0bc29835a4 priv 0x6e3cf6073b9580a401b58b0bc29835a4 localizedkey
rmon event 1 description FATAL(1) owner PMON@FATAL
rmon event 2 description CRITICAL(2) owner PMON@CRITICAL
rmon event 3 description ERROR(3) owner PMON@ERROR
rmon event 4 description WARNING(4) owner PMON@WARNING
rmon event 5 description INFORMATION(5) owner PMON@INFO

vlan 1

ip prefix-list DEFAULT_ONLY seq 5 permit 0.0.0.0/0
route-map RM_FROM_DC1_BORDER_LEAF permit 10
  set ip next-hop unchanged
  set path-selection all advertise
route-map RM_SET_DC2_COMMUNITY_DEFAULT permit 10
  description ### SETTING BGP COMMUNITY ON LOCALLY ORIGINATED DEFAULT ROUTE ###
  set community 65002:9999 additive
route-map RM_TO_DC1_BORDER_LEAF deny 10
  description ### DO NOT ADVERTISE DEFAULT ROUTE TO DC1 ###
  match ip address prefix-list DEFAULT_ONLY
route-map RM_TO_DC1_BORDER_LEAF permit 20
  set ip next-hop unchanged
  set path-selection all advertise
vrf context TENANT01
  vni 300001
  rd auto
  address-family ipv4 unicast
    route-target both auto
    route-target both auto evpn
vrf context TENANT02
  vni 300002
  rd auto
  address-family ipv4 unicast
    route-target both auto
    route-target both auto evpn
vrf context management


interface Vlan1

interface nve1
  no shutdown
  host-reachability protocol bgp
  source-interface loopback1
  member vni 300001 associate-vrf
  member vni 300002 associate-vrf

interface Ethernet1/1
  description ### Link to PE DC2MPLSPE g0/0/0/1 ###
  no switchport
  mtu 8986
  no shutdown

interface Ethernet1/1.10
  description *** VXLAN OSPF PEERING WITH DC2MPLSPE ***
  mtu 8986
  encapsulation dot1q 10
  ip address 10.2.1.1/31
  ip ospf authentication message-digest
  ip ospf authentication-key 3 8e55ae15613d86e9
  ip ospf hello-interval 3
  ip ospf network point-to-point
  ip router ospf 1 area 0.0.0.0
  no shutdown

interface Ethernet1/2
  description ### Link to Spine DC2SPINE01 Eth 1/1 ###
  no switchport
  mtu 9216
  ip address 10.2.1.2/31
  ip ospf authentication message-digest
  ip ospf message-digest-key 1 md5 3 e99f306cc637e510ff865d69fbbd0bb8
  ip ospf network point-to-point
  ip router ospf 1 area 1.1.1.1
  no shutdown

interface Ethernet1/3

interface Ethernet1/4

interface Ethernet1/5

interface Ethernet1/6

interface Ethernet1/7

interface Ethernet1/8

interface Ethernet1/9

interface Ethernet1/10

interface Ethernet1/11

interface Ethernet1/12

interface Ethernet1/13

interface Ethernet1/14

interface Ethernet1/15

interface Ethernet1/16

interface Ethernet1/17

interface Ethernet1/18

interface Ethernet1/19

interface Ethernet1/20

interface Ethernet1/21

interface Ethernet1/22

interface Ethernet1/23

interface Ethernet1/24

interface Ethernet1/25

interface Ethernet1/26

interface Ethernet1/27

interface Ethernet1/28

interface Ethernet1/29

interface Ethernet1/30

interface Ethernet1/31

interface Ethernet1/32

interface Ethernet1/33

interface Ethernet1/34

interface Ethernet1/35

interface Ethernet1/36

interface Ethernet1/37

interface Ethernet1/38

interface Ethernet1/39

interface Ethernet1/40

interface Ethernet1/41

interface Ethernet1/42

interface Ethernet1/43

interface Ethernet1/44

interface Ethernet1/45

interface Ethernet1/46

interface Ethernet1/47

interface Ethernet1/48

interface Ethernet1/49

interface Ethernet1/50

interface Ethernet1/51

interface Ethernet1/52

interface Ethernet1/53

interface Ethernet1/54

interface Ethernet1/55

interface Ethernet1/56

interface Ethernet1/57

interface Ethernet1/58

interface Ethernet1/59

interface Ethernet1/60

interface Ethernet1/61

interface Ethernet1/62

interface Ethernet1/63

interface Ethernet1/64

interface Ethernet1/65

interface Ethernet1/66

interface Ethernet1/67

interface Ethernet1/68

interface Ethernet1/69

interface Ethernet1/70

interface Ethernet1/71

interface Ethernet1/72

interface Ethernet1/73

interface Ethernet1/74

interface Ethernet1/75

interface Ethernet1/76

interface Ethernet1/77

interface Ethernet1/78

interface Ethernet1/79

interface Ethernet1/80

interface Ethernet1/81

interface Ethernet1/82

interface Ethernet1/83

interface Ethernet1/84

interface Ethernet1/85

interface Ethernet1/86

interface Ethernet1/87

interface Ethernet1/88

interface Ethernet1/89

interface Ethernet1/90

interface Ethernet1/91

interface Ethernet1/92

interface Ethernet1/93

interface Ethernet1/94

interface Ethernet1/95

interface Ethernet1/96

interface Ethernet1/97

interface Ethernet1/98

interface Ethernet1/99

interface Ethernet1/100

interface Ethernet1/101

interface Ethernet1/102

interface Ethernet1/103

interface Ethernet1/104

interface Ethernet1/105

interface Ethernet1/106

interface Ethernet1/107

interface Ethernet1/108

interface Ethernet1/109

interface Ethernet1/110

interface Ethernet1/111

interface Ethernet1/112

interface Ethernet1/113

interface Ethernet1/114

interface Ethernet1/115

interface Ethernet1/116

interface Ethernet1/117

interface Ethernet1/118

interface Ethernet1/119

interface Ethernet1/120

interface Ethernet1/121

interface Ethernet1/122

interface Ethernet1/123

interface Ethernet1/124

interface Ethernet1/125

interface Ethernet1/126

interface Ethernet1/127

interface Ethernet1/128

interface mgmt0
  vrf member management
  ip address 1.1.1.13/24

interface loopback0
  description ### ROUTER ID ###
  ip address 10.2.255.2/32
  ip router ospf 1 area 1.1.1.1

interface loopback1
  description ### VTEP ID ###
  ip address 10.2.100.2/32
  ip router ospf 1 area 1.1.1.1
line console
line vty
boot nxos bootflash:/nxos.9.2.2.bin
router ospf 1
  router-id 10.2.255.2
  area 1.1.1.1 range 10.2.100.0/24
  area 1.1.1.1 range 10.2.200.0/24
  area 1.1.1.1 range 10.2.255.0/24
  timers throttle spf 10 100 500
  timers throttle lsa 0 100 500
router bgp 65002
  router-id 10.2.255.2
  log-neighbor-changes
  address-family ipv4 unicast
  address-family l2vpn evpn
  neighbor 10.1.255.2
    remote-as 65001
    log-neighbor-changes
    description ### DC1BORDERLEAF01 ***
    password 3 e99f306cc637e510ff865d69fbbd0bb8
    update-source loopback0
    ebgp-multihop 5
    peer-type fabric-external
    address-family ipv4 unicast
    address-family l2vpn evpn
      send-community
      send-community extended
      route-map RM_FROM_DC1_BORDER_LEAF in
      route-map RM_TO_DC1_BORDER_LEAF out
      rewrite-evpn-rt-asn
  neighbor 10.2.255.5
    remote-as 65002
    password 3 e99f306cc637e510ff865d69fbbd0bb8
    update-source loopback0
    address-family ipv4 unicast
    address-family l2vpn evpn
      send-community
      send-community extended
  vrf TENANT01
    address-family ipv4 unicast
      network 0.0.0.0/0 route-map RM_SET_DC2_COMMUNITY_DEFAULT
  vrf TENANT02
    address-family ipv4 unicast
      network 0.0.0.0/0 route-map RM_SET_DC2_COMMUNITY_DEFAULT
evpn
